FROM openjdk:8-jdk-alpine
VOLUME /tmp
COPY /target/gateway-0.0.1-SNAPSHOT.jar app/app.jar
EXPOSE 8080
CMD ["java", "-Xmx200m", "-jar","/app/app.jar"]
